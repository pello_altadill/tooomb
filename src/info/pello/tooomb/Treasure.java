package info.pello.tooomb;

/**
 * Represents Treasures that player could find in Dungeons
 * @author Peter Class, John Lisp
 * 
 */
public abstract class Treasure {
	private int type;
	private String name;
	private int value;
	public static final int GOLD = 0;
	public static final int WEAPON = 1;
	public static final int POTION = 2;
	
	/**
	 * Constructor for Treasure
	 * @param name
	 * @param value
	 */
	public Treasure (String name, int value) {
		this.name = name;
		this.value = value;
	}
	
	/**
	 * factory method to generate treasures depending on type
	 * @param type
	 * @param name
	 * @param value
	 * @return
	 */
	public static Treasure create(int type, String name, int value) {
	        switch (type) {
	            case GOLD:
	               return new Gold(name,value);
	            case WEAPON:
		           return new Weapon(name,value);
	            case POTION:
		           return new Potion(name,value);
		        default:
	               throw new IllegalArgumentException("Incorrect type code value");
	        }
	    }

	
	/**
	 * @return the type
	 */
	public abstract int getType();
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Treasure [type=" + type + ", name=" + name + ", value=" + value
				+ "]";
	}
	
}