/**
 * 
 */
package info.pello.tooomb;

/**
 * @author luser
 *
 */
public class Potion extends Treasure {

	/**
	 * constructor calls super class
	 * @param name
	 * @param value
	 */
	public Potion(String name, int value) {
		super(name, value);
		// TODO Auto-generated constructor stub
	}

	public int getType () {
		return Treasure.GOLD;
	}
	
	/**
	 * applies only with potion
	 * @return life points
	 */
	public int heal() {
		return getValue() + D6.getD6().roll();
	}
}
