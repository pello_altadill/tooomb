package info.pello.tooomb;
/**
 * 
 */

/**
 * @author luser
 *
 */
public class Wizard extends Character {

	public Wizard (String name) {
		super(name);
	}
	
	@Override
	public int attack(Character character) {
		return this.intelligence + D6.getD6().roll();
	}

	@Override
	public int defend(Character character) {
		// TODO Auto-generated method stub
		return this.intelligence + D6.getD6().roll();
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Wizard";
	}

}
