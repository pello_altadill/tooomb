/**
 * 
 */
package info.pello.tooomb.tests;

import info.pello.tooomb.Maze;
import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

/**
 * Testing class for Maze
 * @author Peter Class
 *
 */
public class MazeTest {

	Maze testedMaze;
	
	@Test
	public void testNew() {
		String name = "Test Maze";
		testedMaze = new Maze(name);
		assertEquals("Test if name is assigned", testedMaze.getName(), name);
	}
	
	/**
	 * we test every single dungeon and by the way 
	 * we check if there is only one exit.
	 * TODO: shall I create a public accessor to Dungeon matrix
	 */
	public void testDungeons () {
		int exit = 0;
		for (int i = 0;i < 10;i++) {
			for (int j=0;j<10;j++) {
				
			}
		}
	}

}
