package info.pello.tooomb;

import java.util.Random;

/**
 * Each of the individual rooms of the maze.
 * We can set the Dungeon with a scary name
 * @author Peter Class
 *
 */
public class Dungeon {
	private static final int MAX_DAMAGE = 6;
	private String name;
	private int damage;
	private Random random = new Random();
	private boolean hasExit;
	
	/**
	 * default constructor, dungeon is baptized
	 * with a generic Dark Dungeon name. 
	 */
	public Dungeon () {
		name = "Dark Dungeon";
		init();
	}


	/**
	 * constructor with a name for the dungeon
	 * @param name
	 */
	public Dungeon(String name) {
		this.name = name;
		init();
	}
	
	/**
	 * inits Dungeon parameters
	 */
	private void init() {
		damage = random.nextInt(MAX_DAMAGE);
		hasExit = false;
	}


	/**
	 * @return  name of the Dungeon
	 */
	public String getName () {
		return name;
	}
	
	/**
	 * @param hasExit is set to true
	 */
	public void setHasExit() {
		this.hasExit = true;
	}


	/**
	 * tells is Dungeon has an exit or not
	 * @return
	 */
	public boolean isExit() {
		// TODO Auto-generated method stub
		return hasExit;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Dungeon [name=" + name + ", damage=" + damage 
				+  ", hasExit=" + hasExit + "]";
	}

	/**
	 * 
	 * @return total damage points
	 */
	public int getDamage() {
		// TODO Auto-generated method stub
		return damage;
	}
	
}
