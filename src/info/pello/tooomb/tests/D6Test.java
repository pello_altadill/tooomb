package info.pello.tooomb.tests;

import info.pello.tooomb.D6;
import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Test;

/**
 * Testing class for Maze
 * @author Peter Class
 *
 */
public class D6Test {

	/**
	 * we test the dice rolling 100 times
	 */
	@Test
	public void testRoll() {
		
		for (int i = 0;i < 100;i++) {
				assertTrue(D6.getD6().roll() >= 0 && D6.getD6().roll() <= 5);
		}
	}

}
