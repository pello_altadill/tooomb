/**
 * 
 */
package info.pello.tooomb;
/**
 * @author Peter Class
 *
 */
public class Gold extends Treasure {
	
	/**
	 * constructor calls super class
	 * @param name
	 * @param value
	 */
	public Gold(String name, int value) {
		super(name, value);
		// TODO Auto-generated constructor stub
	}

	public int getType () {
		return Treasure.GOLD;
	}
	
}