/**
 * 
 */
package info.pello.tooomb;

/**
 * @author Peter Class
 *
 */
public abstract class Character {

	protected String name;
	protected int strength;
	protected int speed;
	protected int intelligence;
	protected int life;
	public static final int BARBARIAN = 0;
	public static final int ELF = 1;
	public static final int DWARF = 2;
	public static final int WIZARD = 3;
	
	/**
	 * default constructor
	 * @param name
	 */
	public Character(String name) {
		this.name = name;
		initialize();
	}
	
	/**
	 * initializes attributes
	 */
	private void initialize () {
		this.strength = D6.getD6().roll();
		this.speed = D6.getD6().roll();
		this.intelligence = D6.getD6().roll();
		this.life = strength * 2;
	}
	/**
	 * factory method to create Character instances based on type
	 * @param type
	 * @param name
	 * @return
	 */
	public static Character newType (int type, String name) {
		switch (type) {
		case BARBARIAN:	return new Barbarian(name);
		case ELF:	return new Elf(name);
		case DWARF:	return new Dwarf(name);
		case WIZARD:	return new Wizard(name);
		default: throw new RuntimeException("Incorrect Character");
						
		}
	}
	
	/**
	 * the character attacking roll
	 * @return  attack points
	 */
	public abstract int attack(Character character);
	
	/**
	 * the character defense roll
	 * @return defense points
	 */
	public abstract int defend(Character character);
	
	/**
	 * given type code returns Its name
	 * @return the name of player type
	 */
	public abstract String getType();
	
	/* 
	 * shows character info
	 */
	public String info () {
		return this.name +" [strength=" + this.strength + ", speed=" + this.speed
				+ ", intelligence=" + this.intelligence + ", type=" + getType()
				+ ", life=" + this.life + "]";
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * takes some life points
	 * @param points to take
	 */
	public void takeLife(int points) {
		this.life -= points;
	}

	/**
	 * @return remaining life points
	 */
	public int getLife() {
		return this.life;
	}

}
