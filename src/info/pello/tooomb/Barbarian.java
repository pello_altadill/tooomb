/**
 * 
 */
package info.pello.tooomb;

/**
 * @author luser
 *
 */
public class Barbarian extends Character {

	public Barbarian(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see info.pello.tooomb.CharacterType#attack(info.pello.tooomb.Character)
	 */
	@Override
	public int attack(Character character) {
		return this.strength + D6.getD6().roll();
	}

	/* (non-Javadoc)
	 * @see info.pello.tooomb.CharacterType#defend(info.pello.tooomb.Character)
	 */
	@Override
	public int defend(Character character) {
		// TODO Auto-generated method stub
		return (this.speed + this.strength)/2 + D6.getD6().roll();
	}

	/* (non-Javadoc)
	 * @see info.pello.tooomb.CharacterType#getType(info.pello.tooomb.Character)
	 */
	@Override
	public String getType() {
		return "Barbarian";
	}

}
