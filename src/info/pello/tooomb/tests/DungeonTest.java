/**
 * 
 */
package info.pello.tooomb.tests;

import info.pello.tooomb.Dungeon;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * tests for Dungeon class
 * @author John Lisp
 *
 */
public class DungeonTest {

	private Dungeon testedDungeon;


	@Test
	/**
	 * test if newly created Dungeons have expected name
	 */
	public void testNew() {
		testedDungeon = new Dungeon();
		assertEquals("Default dungeon name ok.",testedDungeon.getName(),"Dark Dungeon");
		String name = "Terror Dungeon";
		testedDungeon = new Dungeon(name);
		assertEquals("Set dungeon name and is ok.",testedDungeon.getName(),name);
	}
	
	@Test
	/**
	 * test if Dungeons newly created Dungeons haven't exit
	 */
	public void testNoExit() {
			testedDungeon = new Dungeon();
			assertFalse("Dungeons have no exit",testedDungeon.isExit());
	}
	
	@Test
	/**
	 * test if Dungeons have damage points within limits.
	 */
	public void testDamage() {
		for (int i = 0; i < 100; i++) {
			testedDungeon = new Dungeon();
			assertTrue("Dungeon damage between 0 and 6",testedDungeon.getDamage()>= 0 && testedDungeon.getDamage() < 6);
		}
	}

}
