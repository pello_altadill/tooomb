package info.pello.tooomb;

import java.util.Random;

/**
 * The Maze, where the action takes place
 * @author John Lisp
 *
 */
public class Maze {

	private String name;
	private Dungeon[][] dungeons = new Dungeon[10][10];
	private Random random = new Random();
	private int currentX;
	private int currentY;

	/**
	 * @param name for the maze
	 */
	public Maze(String name) {
		this.name = name;
		buildDungeons();
	}

	/**
	 * @return the name of the maze
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * generates all the Dungeons
	 */
	private void buildDungeons () {
		for (int i= 0;i<10;i++)
			for (int j=0;j<10;j++)
				dungeons[i][j] = new Dungeon("Dark Dungeon");
		
		setExitDungeon();
		setInitialDungeon();
	}

	/**
	 * When the game starts,
	 * Put the player in this dungeon
	 */
	private void setInitialDungeon() {
		// TODO Auto-generated method stub
		currentX = random.nextInt(10);
		currentY = random.nextInt(10);
	}

	/**
	 * sets one of the Dungeons as exit point
	 */
	private void setExitDungeon() {
		dungeons[random.nextInt(10)][random.nextInt(10)].setHasExit();
	}

	/**
	 * just says if given a direction from current position
	 * the move is possible
	 * @param direction
	 * @return
	 */
	public boolean moveIsNotPossible(String direction) {
		
		if (direction.equals("n") && currentY==0) { return true; }
		if (direction.equals("s") && currentY==9) { return true; }
		if (direction.equals("w") && currentX==0) { return true; }
		if (direction.equals("e") && currentX==9) { return true; }
		// TODO Auto-generated method stub
		return false;
	}

	public void move(String option) {
		// TODO Auto-generated method stub
		switch (option) {
			case "n" :	currentY--;
						break;
			case "s" :	currentY++;
						break;
			case "w" :	currentX--;
						break;
			case "e" :	currentX++;
						break;
			default:	break;
		}
	}

	/**
	 * returns current Dungeon
	 * @return
	 */
	public Dungeon currentDungeon() {
		// TODO Auto-generated method stub
		return dungeons[currentY][currentX];
	}
	
	/**
	 * tells is current Dungeon has an exit or not
	 * @return
	 */
	public boolean isExit() {
		// TODO Auto-generated method stub
		return currentDungeon().isExit();
	}
	
	/**
	 * returns damage points of current dungeon
	 * @return
	 */
	public int getDamage() {
		// TODO Auto-generated method stub
		return currentDungeon().getDamage();
	}
		
	/**
	 * dumps info about current situation
	 * @return
	 */
	public String currentInfo () {
		return "Current: " + currentX + ":" + currentY + 
		"\ndungeon: " + currentDungeon().toString();
	}
	
	/**
	 * draws a String with an ascii map of
	 * the Dungeon.
	 * @return
	 */
	public String showMap () {
		String map = "  ";
		
		// paint X coords
		for (int k=0;k<10;k++) {
			map += " " + k + " ";
		}
		
		for (int i= 0;i<10;i++) {
			map += "\n" + i + " ";
			for (int j=0;j<10;j++) {				if (currentX==j && currentY==i)
					map += "[@]";
				else
					map += "[ ]";
			}
		}

		map += "\n  ";
		// paint X coords
		for (int k=0;k<10;k++) {
			map += " " + k + " ";
		}

		return map;
	}
}
