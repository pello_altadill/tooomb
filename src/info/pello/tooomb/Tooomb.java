package info.pello.tooomb;

import java.util.Scanner;

/**
 * the class that takes you deep inside nightmare
 * no one will hear you screaming. rest in pieces.
 * @author float
 */
public class Tooomb {
	public static void main (String args[]) {
		Maze maze = new Maze("Test Maze");
		String option = "";
		Scanner reader = new Scanner(System.in);
		int moves = 0;
		Character player;
		String name = "";
		int characterType = 0;
		
		// Choose character data
		System.out.println("Please choose a name");
		name = reader.next();
		System.out.println("Please choose character type:");
		System.out.println(" 0: Barbarian, 1: Elf, 2: Dwarf, 3: Wizard");
		characterType = reader.nextInt();
		player = Character.newType(characterType, name);
		
		do {
			do {
				System.out.println("Player: " + player.info() + "\n" + " Moves: " + moves);
				System.out.println(maze.showMap());
				System.out.println("Choose direction: ");
				System.out.println("N,S,W,E");
				option = reader.next().toLowerCase();
			
			} while (maze.moveIsNotPossible(option));
				maze.move(option);
				System.out.println(maze.currentInfo());

				player.takeLife(maze.getDamage());
				if (player.getLife() <= 0) {
					System.out.println("You are dead");
					System.exit(1);
				}
				
				moves++;
				
			if (maze.isExit()) {
				System.out.println("You are out!!");
				System.out.println("Total moves: " + moves);
				System.exit(1);
			} else {
				System.out.println("Not exit");
			}
			
		} while (option != "q");
		System.out.println("Tooomb (C) Bubble Telecom - 2013");
	}
}
