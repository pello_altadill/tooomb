
package info.pello.tooomb;

import java.util.Random;

/**
 * a class that behaves like a D6 dice
 * following Singleton pattern
 * @author Peter Class
 *
 */
public class D6 {
	Random random = new Random();
	private static D6 d6;
	
	/**
	 * private constructor
	 */
	private D6 () {
		
	}
	
	/**
	 * this gives access to unique instance of D6
	 * @return
	 */
	public static D6 getD6 () {
		if (d6 == null) {
			d6 = new D6();
		}
		
		return d6;
	}
	
	/**
	 * roll the D6 dice, gives a number between 0 and 5
	 * @return
	 */
	public int roll () {
		return random.nextInt(6);
	}

}
