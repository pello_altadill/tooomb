/**
 * 
 */
package info.pello.tooomb;
/**
 * @author Peter Class
 *
 */
public class Weapon extends Treasure {

	/**
	 * constructor calls super class
	 * @param name
	 * @param value
	 */
	public Weapon(String name, int value) {
		super(name, value);
		// TODO Auto-generated constructor stub
	}

	public int getType () {
		return Treasure.GOLD;
	}
	
	/**
	 * applies only for weapons
	 * @return random number of damage
	 */
	public int hitPoints () {
		return D6.getD6().roll()/2;
	}
	
}